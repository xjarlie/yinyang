let ticks = 0;
let paused = true;

document.body.onload = () => { main(); };

const canvas = document.querySelector('#canvas');
const ctx = canvas.getContext('2d');

const pauseBtn = document.querySelector('#pauseBtn');
const resetBtn = document.querySelector('#resetBtn');


const grid = [];
const gridWidth = 20;
const gridHeight = 20;

const gridSpaceW = ctx.canvas.width / gridWidth;
const gridSpaceH = ctx.canvas.height / gridHeight;

const colorA = 'aquamarine';
const colorB = 'coral';

const ballRadius = 10;
const ballSpeed = 1000;
let blocksA = gridWidth*gridHeight*0.5;

function randomVec() {
    return polarToCartesian({
        angle: ((Math.PI / 4) * Math.random()) + Math.PI/8 + Math.random() * 4,
        magnitude: 1
    });
}

const ballA = {
    x: ctx.canvas.width * 0.75,
    y: ctx.canvas.height * 0.5,
    dx: 0,
    dy: 0,
    id:1
}

const ballB = {
    x: ctx.canvas.width * 0.25,
    y: ctx.canvas.height * 0.5,
    dx: 0,
    dy: 0,
    id: -1
}

pauseBtn.onclick = () => {
    togglePause();
}

resetBtn.onclick = () => {
    togglePause(true);

    setup();

    drawGrid();
    drawBalls();

    togglePause(false);
}

ctx.canvas.onclick = (e) => {

    const rect = ctx.canvas.getBoundingClientRect();

    const gridX = Math.floor((e.clientX - rect.left) / (ctx.canvas.offsetWidth / gridWidth));
    const gridY = Math.floor((e.clientY - rect.top) / (ctx.canvas.offsetHeight / gridHeight));
    console.log(gridX, gridY);

    grid[gridX][gridY] *= -1;

}

function togglePause(val) {

    if (val !== undefined) {
        paused = val;
    } else {
        paused = !paused;
    }
    
    if (paused) {
        pauseBtn.querySelector('#pauseIcon').style.display = 'none';
        pauseBtn.querySelector('#playIcon').style.display = 'block';
    } else {
        pauseBtn.querySelector('#pauseIcon').style.display = 'block';
        pauseBtn.querySelector('#playIcon').style.display = 'none';
    }
}


function main() {
    setup();
    requestAnimationFrame(tick);
    paused = false;
}

function setup() {
    for (let i = 0; i < gridWidth; i++) {
        grid[i] = [];
        for (let j = 0; j < gridHeight; j++) {
            grid[i][j] = i < gridWidth/2 ? 1 : -1;
        }
    }

    const rA = randomVec();
    const rB = randomVec();

    ballA.dx = rA.x;
    ballA.dy = rA.y;
    ballB.dx = -rB.x;
    ballB.dy = -rB.y;

    ballA.x = ctx.canvas.width * 0.75;
    ballA.y = ctx.canvas.height * 0.5;

    ballB.x = ctx.canvas.width * 0.25;
    ballB.y = ctx.canvas.height * 0.5;


}

function drawGrid() {
    const rectWidth = ctx.canvas.width / gridWidth;
    const rectHeight = ctx.canvas.height / gridHeight;
    for (let i = 0; i < gridWidth; i++) {
        for (let j = 0; j < gridHeight; j++) {

            const canvasX = i * rectWidth;
            const canvasY = j * rectHeight;

            ctx.fillStyle = grid[i][j] === 1 ? colorA : colorB;
            //ctx.fillStyle = getRandomColor();
            //ctx.fillStyle = colorB
            ctx.fillRect(canvasX, canvasY, rectWidth, rectHeight);

        }
    }
}
function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

function drawBalls() {
    ctx.fillStyle = colorA;
    ctx.beginPath();
    ctx.arc(ballA.x, ballA.y, ballRadius, 0, 2*Math.PI);
    ctx.fill();

    ctx.fillStyle = colorB;
    ctx.beginPath();
    ctx.arc(ballB.x, ballB.y, ballRadius, 0, 2*Math.PI);
    ctx.fill();
}

let lastCollision = 0;

// function renderLoop() {

//     if (!paused) {
//         drawGrid();
//         drawBalls();
//     }

//     requestAnimationFrame(renderLoop);
// }

let previousTimestamp;

function tick(timestamp) {
    if (previousTimestamp === undefined) previousTimestamp = timestamp;
    const elapsed = timestamp - previousTimestamp;
    previousTimestamp = timestamp;

    if (paused) {
        requestAnimationFrame(tick);
        //setTimeout(tick, 7);
        return;
    }


    console.warn('TICK');

    const smallVariation = (Math.random() / 10) * (Math.sign(Math.random() - 0.5));

    // Collisions
    if (ballA.x + ballRadius >= ctx.canvas.width) {
        ballA.x = ctx.canvas.width - ballRadius;
        ballA.dx = -ballA.dx;
    } else
    if (ballA.x - ballRadius <= 0) {
        ballA.x = ballRadius;
        ballA.dx = -ballA.dx;
    } else
    if (ballA.y + ballRadius >= ctx.canvas.height) {
        ballA.y = ctx.canvas.height - ballRadius;
        ballA.dy = -ballA.dy;
    } else 
    if (ballA.y - ballRadius <= 0) {
        ballA.y = ballRadius;
        ballA.dy = -ballA.dy;
    }

    if (ballB.x + ballRadius >= ctx.canvas.width) {
        ballB.x = ctx.canvas.width - ballRadius;
        ballB.dx = -ballB.dx;
    } else
    if (ballB.x - ballRadius <= 0) {
        ballB.x = ballRadius;
        ballB.dx = -ballB.dx;
    } else
    if (ballB.y + ballRadius >= ctx.canvas.height) {
        ballB.y = ctx.canvas.height - ballRadius;
        ballB.dy = -ballB.dy;
    } else 
    if (ballB.y - ballRadius <= 0) {
        ballB.y = ballRadius;
        ballB.dy = -ballB.dy;
    }

    
    // Grid changing
    const collideA = detectGridSpaces(ballA);
    const collideB = detectGridSpaces(ballB);


    switch (collideA[0][0]) {
        case 'left':
            ballA.dx = -ballA.dx;
            break;
        case 'right':
            ballA.dx = -ballA.dx;
            break;
        case 'top':
            ballA.dy = -ballA.dy;
            break;
        case 'bottom':
            ballA.dy = -ballA.dy;
            break;
        default:
            break;
    }
    const agX = collideA[1][0]?.[0];
    const agY = collideA[1][0]?.[1];
    if (agX !== undefined && agY !== undefined) {
        grid[agX][agY] = ballA.id * -1;
        blocksA--;
    }


    switch (collideB[0][0]) {
        case 'left':
            ballB.dx = -ballB.dx;
            break;
        case 'right':
            ballB.dx = -ballB.dx;
            break;
        case 'top':
            ballB.dy = -ballB.dy;
            break;
        case 'bottom':
            ballB.dy = -ballB.dy;
            break;
        default:
            break;
    }
    const bgX = collideB[1][0]?.[0];
    const bgY = collideB[1][0]?.[1];
    if (bgX !== undefined && bgY !== undefined) {
        grid[bgX][bgY] = ballB.id * -1
        blocksA++;
    };

    // Movement
    const elapsedS = elapsed / 1000;
    ballA.x += ballA.dx * ballSpeed * elapsedS;
    ballA.y += ballA.dy * ballSpeed * elapsedS;

    ballB.x += ballB.dx * ballSpeed * elapsedS;
    ballB.y += ballB.dy * ballSpeed * elapsedS;

    

    //const collideB = detectGridSpaces(ballB);

    
    drawGrid();
    drawBalls();

    ticks++;
    requestAnimationFrame(tick);
    //setTimeout(tick, 1);
}

function detectGridSpaces(ball) {

    const positions = [

        [ball.x + ballRadius, ball.y],
        [ball.x + Math.sqrt(0.5*Math.pow(ballRadius,2)), ball.y + Math.sqrt(0.5*Math.pow(ballRadius,2))],
        [ball.x, ball.y + ballRadius],
        [ball.x - Math.sqrt(0.5*Math.pow(ballRadius,2)), ball.y + Math.sqrt(0.5*Math.pow(ballRadius,2))],
        [ball.x - ballRadius, ball.y],
        [ball.x - Math.sqrt(0.5*Math.pow(ballRadius,2)), ball.y - Math.sqrt(0.5*Math.pow(ballRadius,2))],
        [ball.x, ball.y - ballRadius],
        [ball.x + Math.sqrt(0.5*Math.pow(ballRadius,2)), ball.y - Math.sqrt(0.5*Math.pow(ballRadius,2))]

    ]

    const spaces = [];

    for (const pos of positions) {
        const gsX = Math.floor(gridWidth * pos[0] / ctx.canvas.width).clamp(0, grid.length-1);
        const gsY = Math.floor(gridHeight * pos[1] / ctx.canvas.height).clamp(0, grid.length-1);
        
        if (grid[gsX][gsY] === ball.id) {
            if (!spaces.includes([gsX, gsY])) {
                spaces.push([gsX, gsY]);
            }
        }
        //grid[gsX][gsY] = ball.id * -1;

    }

    const collisions = [];
    const collisionSpaces = [];

    for (const space of spaces) {

        const rect = {
            x: space[0] * gridSpaceW,
            y: space[1] * gridSpaceH,  
            w: gridSpaceW,
            h: gridSpaceH
        }

        const circle = {
            x: ball.x,
            y: ball.y
        }

        // if (isColliding(circle, rect)) {
        //     collidingSpaces.push(space);
        //     grid[space[0]][space[1]] = ball.id * -1;
        // }

        const collision = getCollision(circle, rect);
        if (collision === 0) {
            continue;
        }

        collisions.push(collision);
        collisionSpaces.push(space);

    }

    return [collisions, collisionSpaces];

}

// Collision types:
// 0: no collision
// 1: horizontal collision
// 2: vertical collision

function getCollision(circle = { x: 0, y: 0 }, rect = { x: 0, y: 0, w: 0, h: 0 }) {

    let testX = circle.x;
    let testY = circle.y;

    let direction = '';

    if (circle.x < rect.x) {
        testX = rect.x; // left
        direction = 'left';
    } else if (circle.x > rect.x + rect.w) {
        testX = rect.x + rect.w; // right
        direction = 'right';
    }

    if (circle.y < rect.y) {
        testY = rect.y; // top
        direction = 'top';
    } else if (circle.y > rect.y + rect.h) {
        testY = rect.y + rect.h; // bottom 
        direction = 'bottom';
    }

    const distX = circle.x - testX;
    const distY = circle.y - testY;
    
    const distance = Math.sqrt( (distX*distX) + (distY*distY) );

    if (distance <= ballRadius) {
        return direction;
    }

    return 0;

    // if (distX === 0 || distY === 0) return 1;

    // return 0;

    // var dx = distX - rect.w / 2;
    // var dy = distY - rect.h / 2;
    // return (dx * dx + dy * dy <= (circle.r * circle.r)) ? 2 : 0;
}


function polarToCartesian({angle, magnitude}) {
    return {
        x: magnitude * Math.cos(angle),
        y: magnitude * Math.sin(angle)
    }
}
// 0-> pi, 

function cartesianToPolar({x, y}) {
    return {
        magnitude: Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2)),
        angle: Math.atan(y / x)
    }
}

function dotProduct(v1, v2) {
    return (v1.x * v2.x) + (v1.y * v2.y);
}

Number.prototype.clamp = function(min, max) {
    return Math.min(Math.max(this, min), max);
  };